----------------------------------------------------------------------------------
-- Company: 		Mas elettronica
-- Engineer: 		Mascetti
-- 
-- Create Date:   	 21/04/2023
-- Design Name: 	 
-- Module Name:       
-- Project Name: 	 
-- Target Devices: 	 Lattice LCMXO2_256 
-- Tool versions: 	 DIAMOND 3.11
-- Description: 
--
-- Dependencies: 
--
-- Revision: 			1.0
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
library machxo;
use machxo.all;

entity cpld_portatile_top is
		
port(
	rst_n				: in std_logic;
	gpio1_9				: in std_logic;		
	gpio1_10			: in std_logic;
	gpio1_11			: in std_logic;
	gpio1_12			: in std_logic;
	gpio2_1				: in std_logic;
	gpio2_2				: in std_logic;
	gpio2_3				: in std_logic;
	gpio2_10			: in std_logic;
	gpio2_13			: in std_logic;
	gpio2_14			: in std_logic;
	gpio3_24			: in std_logic;
	gpio3_25			: in std_logic;
	gpio3_26			: in std_logic;
	gpio3_27			: in std_logic;
	gpio4_29			: in std_logic;
	irq02				: in std_logic;										
	irq08				: in std_logic;										
	proc_spi_clk		: in std_logic;										
	proc_spi_miso		: in std_logic;
	proc_spi_mosi		: in std_logic;										
	uart1_ctsb			: in std_logic;		
	cpld_rstn			: in std_logic;										--system reset
	gpio3_28			: in std_logic;
	gpio3_29			: in std_logic;
	gpio3_30			: in std_logic;
	gpio4_30			: in std_logic;
	gpio4_31			: in std_logic;
	hreset_n			: out std_logic;
	mxm_waken			: in std_logic;
	poreset_n			: out std_logic;
	pwr_rst_n			: in std_logic;
	reset_req_n			: in std_logic;			
	ddr_rst_n			: out std_logic;										
	irq00				: in std_logic;
	m2_rstn				: out std_logic;									
	mxm_rstn			: out std_logic;									
	nand_cs_n			: out std_logic;
	nand_rb_n			: in std_logic;
	nor_cs_n			: out std_logic;	
	nor_rst_n			: out std_logic;	
	pcie_bridge_rstn    : out std_logic;	
	pdetect 			: out std_logic;	
	pe_waken            : out std_logic;	
	sata3_wake_n 		: out std_logic;	
	sata_rstn           : out std_logic;	
	usb3_pewakeb        : out std_logic;	
	expander_rstn        : out std_logic;
	vcore_en	        : in std_logic;	
	wlan_wakeup			: in std_logic;	
	cfg_vbank0			: out std_logic;										--system reset
	cfg_vbank1			: out std_logic;										--system reset
	cfg_vbank2			: out std_logic;										--system reset
	prsnt_rn		    : in std_logic;	
	clk_50mhz 			: in std_logic;										
	gpio3_0				: in std_logic;	
	gpio3_1				: in std_logic;	
	gpio3_2				: in std_logic;	
	gpio3_3				: in std_logic;	
	gpio3_4				: in std_logic;	
	gpio3_5				: in std_logic;	
	gpio3_6				: in std_logic;	
	gpio3_7				: in std_logic;	
	gpio4_5				: in std_logic;	
	gpio4_6				: in std_logic;	
	ifc_clk				: in std_logic;	
	ifc_cs0_n			: in std_logic;	
	ifc_cs1_n			: in std_logic;	
	ifc_cs2_n			: in std_logic;	
	ifc_rb0_n			: out std_logic;	
	ifc_rb1_n			: out std_logic;	
	osc_out				: out std_logic;
	irq01				: in std_logic										

	 );
end cpld_portatile_top;



architecture rtl of cpld_portatile_top is	
signal ihreset_n : std_logic;
signal i_pwr_rst_n,osc_int_2 : std_logic;
signal pwr_reset_outn : std_logic;
signal osc_int,stm_clk : std_logic;
signal   osc_clk_cnt : std_logic_vector(7 downto 0);
signal rstn_i,poreset_n_i : std_logic;
signal reset_states : integer range 0 to 3;
signal rst_cnt : integer range 0 to 1048576;
constant rst_cnt_max_200ms : integer := 50000;
constant rst_cnt_max_10ms : integer := 8000;
COMPONENT OSCC
 PORT (OSC:OUT std_logic);
END COMPONENT;


begin

--gpio1_9				: in std_logic;		
	--gpio1_10			: in std_logic;
	--gpio1_11			: in std_logic;
	--gpio1_12			: in std_logic;
	--gpio2_1				: in std_logic;
	--gpio2_2				: in std_logic;
	--gpio2_3				: in std_logic;
	--gpio2_10			: in std_logic;
	--gpio2_13			: in std_logic;
	--gpio2_14			: in std_logic;
	--gpio3_24			: in std_logic;
	--gpio3_25			: in std_logic;
	--gpio3_26			: in std_logic;
	--gpio3_27			: in std_logic;
	--gpio4_29			: in std_logic;

OSCInst0: OSCC  -- 18 to 26Mhz
 PORT MAP (
 OSC => osc_int_2
 );

osc_int <= osc_int_2; --clk_50mhz;

rstn_i <= pwr_rst_n;
poreset_n <= poreset_n_i;

--ihreset_n <= rstn_i and hreset_n;
--ihreset_n <= hreset_n;
hreset_n <= ihreset_n;
expander_rstn <= poreset_n_i; --ihreset_n and gpio1_9;

ddr_rst_n  <= poreset_n_i;
m2_rstn	   <= poreset_n_i; 			--ihreset_n and gpio1_10;									
mxm_rstn   <= poreset_n_i; 		--ihreset_n and gpio1_11;
nor_rst_n	<= poreset_n_i;
pcie_bridge_rstn  <= poreset_n_i;  --ihreset_n and gpio2_1;
sata_rstn  <= poreset_n_i; 		--ihreset_n and gpio2_2;
--hreset_n <= pwr_rst_n


nand_cs_n	<= ifc_cs1_n;
nor_cs_n <= ifc_cs0_n;
ifc_rb0_n	<= '1';	
ifc_rb1_n	<= nand_rb_n;
cfg_vbank0	<= '0';										
cfg_vbank1	<= '0';										
cfg_vbank2	<= '1';										


osc_out <= stm_clk; --osc_int_2; 	
process (rstn_i,osc_int)
begin
if (rstn_i = '0') then osc_clk_cnt <= (others => '0');
elsif(rising_edge(osc_int)) 
	then 
		if (osc_clk_cnt = X"FF") 
			then osc_clk_cnt <= (others => '0');
			else osc_clk_cnt <= osc_clk_cnt + X"01";
		end if;
end if;
end process;
stm_clk <= osc_clk_cnt(4); --osc_clk_cnt(5); 

--reset_state_m : process (stm_clk)
--begin
--if rising_edge(stm_clk)
	--then 
		--CASE reset_states is
		--WHEN 0 => if (rstn_i='0')
				  --then
					--reset_states <= 0;
					--rst_cnt <= 0;
					--poreset_n_i <= '0';
				  --else reset_states <= reset_states + 1;
				  --end if;
		--WHEN 1 => poreset_n_i <= '0';-- era '1'
				  --reset_states <= reset_states + 1;
		--WHEN 2 => if (rstn_i='0')
				  --then reset_states <= 0;
					   --poreset_n_i <= '0';
				  --elsif (ihreset_n = '0')
					   --then reset_states <= reset_states + 1;
				  --end if;
		--WHEN 3 => if (rstn_i='0')
				  --then reset_states <= 0;
					   --poreset_n_i <= '0';
				  --elsif (rst_cnt = rst_cnt_max) 
					 --then poreset_n_i <= '1';
						  --reset_states <=3;
					 --else  rst_cnt<= rst_cnt + 1;
				  --end if;
		--end case;
--end if;
--end process;
reset_state_m : process (stm_clk,rstn_i)
begin
 if (rstn_i='0')
   then reset_states <= 0;
		 rst_cnt <= 0;
		 poreset_n_i <= '0';
		 ihreset_n <= '0';
		 
   elsif rising_edge(stm_clk)
	then 
		CASE reset_states is
		WHEN 0 => poreset_n_i <= '0';
				  ihreset_n <= '0'; 
				  rst_cnt <= 0;
				  reset_states <= reset_states + 1;
		WHEN 1 => if (rst_cnt = rst_cnt_max_10ms) 
					 then poreset_n_i <= '0';
						  reset_states <=reset_states + 1;
						  rst_cnt <= 0;
						  ihreset_n <= '0'; 
					 else  rst_cnt<= rst_cnt + 1;
						   poreset_n_i <= '1';
						   ihreset_n <= '0'; 
				  end if;
		WHEN 2 => if (rst_cnt = rst_cnt_max_10ms) 
					 then poreset_n_i <= '1';
						  reset_states <=reset_states + 1;
						  rst_cnt <= 0;
						  ihreset_n <= '0'; 
					 else  rst_cnt<= rst_cnt + 1;
						   poreset_n_i <= '0';
						   ihreset_n <= '0'; 
				  end if;
		WHEN 3 => if (rst_cnt = rst_cnt_max_200ms) 
					 then poreset_n_i <= '1';
						  reset_states <=3;
						  ihreset_n <= '1';
					 else  rst_cnt<= rst_cnt + 1;
						  ihreset_n <= '0';
						  poreset_n_i <= '1';
				  end if;
		end case;
end if;
end process;				       

                                     
end RTL;