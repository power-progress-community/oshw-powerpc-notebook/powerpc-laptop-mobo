[ActiveSupport PAR]
; Global primary clocks
GLOBAL_PRIMARY_USED = 1;
; Global primary clock #0
GLOBAL_PRIMARY_0_SIGNALNAME = osc_outz;
GLOBAL_PRIMARY_0_DRIVERTYPE = SLICE;
GLOBAL_PRIMARY_0_LOADNUM = 15;
; # of global secondary clocks
GLOBAL_SECONDARY_USED = 0;
; I/O Bank 0 Usage
BANK_0_USED = 21;
BANK_0_AVAIL = 41;
BANK_0_VCCIO = 3.3V;
BANK_0_VREF1 = NA;
BANK_0_VREF2 = NA;
; I/O Bank 1 Usage
BANK_1_USED = 4;
BANK_1_AVAIL = 37;
BANK_1_VCCIO = 3.3V;
BANK_1_VREF1 = NA;
BANK_1_VREF2 = NA;
