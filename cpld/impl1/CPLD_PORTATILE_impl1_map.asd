[ActiveSupport MAP]
Device = LCMXO256C;
Package = TQFP100;
Performance = 5;
LUTS_avail = 256;
LUTS_used = 54;
FF_avail = 256;
FF_used = 30;
INPUT_LVTTL33 = 4;
OUTPUT_LVTTL33 = 21;
IO_avail = 78;
IO_used = 25;
