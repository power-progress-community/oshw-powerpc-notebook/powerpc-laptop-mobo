--
-- Synopsys
-- Vhdl wrapper for top level design, written on Tue Oct 15 20:41:03 2024
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity wrapper_for_cpld_portatile_top is
   port (
      rst_n : in std_logic;
      gpio1_9 : in std_logic;
      gpio1_10 : in std_logic;
      gpio1_11 : in std_logic;
      gpio1_12 : in std_logic;
      gpio2_1 : in std_logic;
      gpio2_2 : in std_logic;
      gpio2_3 : in std_logic;
      gpio2_10 : in std_logic;
      gpio2_13 : in std_logic;
      gpio2_14 : in std_logic;
      gpio3_24 : in std_logic;
      gpio3_25 : in std_logic;
      gpio3_26 : in std_logic;
      gpio3_27 : in std_logic;
      gpio4_29 : in std_logic;
      irq02 : in std_logic;
      irq08 : in std_logic;
      proc_spi_clk : in std_logic;
      proc_spi_miso : in std_logic;
      proc_spi_mosi : in std_logic;
      uart1_ctsb : in std_logic;
      cpld_rstn : in std_logic;
      gpio3_28 : in std_logic;
      gpio3_29 : in std_logic;
      gpio3_30 : in std_logic;
      gpio4_30 : in std_logic;
      gpio4_31 : in std_logic;
      hreset_n : out std_logic;
      mxm_waken : in std_logic;
      poreset_n : out std_logic;
      pwr_rst_n : in std_logic;
      reset_req_n : in std_logic;
      ddr_rst_n : out std_logic;
      irq00 : in std_logic;
      m2_rstn : out std_logic;
      mxm_rstn : out std_logic;
      nand_cs_n : out std_logic;
      nand_rb_n : in std_logic;
      nor_cs_n : out std_logic;
      nor_rst_n : out std_logic;
      pcie_bridge_rstn : out std_logic;
      pdetect : out std_logic;
      pe_waken : out std_logic;
      sata3_wake_n : out std_logic;
      sata_rstn : out std_logic;
      usb3_pewakeb : out std_logic;
      expander_rstn : out std_logic;
      vcore_en : in std_logic;
      wlan_wakeup : in std_logic;
      cfg_vbank0 : out std_logic;
      cfg_vbank1 : out std_logic;
      cfg_vbank2 : out std_logic;
      prsnt_rn : in std_logic;
      clk_50mhz : in std_logic;
      gpio3_0 : in std_logic;
      gpio3_1 : in std_logic;
      gpio3_2 : in std_logic;
      gpio3_3 : in std_logic;
      gpio3_4 : in std_logic;
      gpio3_5 : in std_logic;
      gpio3_6 : in std_logic;
      gpio3_7 : in std_logic;
      gpio4_5 : in std_logic;
      gpio4_6 : in std_logic;
      ifc_clk : in std_logic;
      ifc_cs0_n : in std_logic;
      ifc_cs1_n : in std_logic;
      ifc_cs2_n : in std_logic;
      ifc_rb0_n : out std_logic;
      ifc_rb1_n : out std_logic;
      osc_out : out std_logic;
      irq01 : in std_logic
   );
end wrapper_for_cpld_portatile_top;

architecture rtl of wrapper_for_cpld_portatile_top is

component cpld_portatile_top
 port (
   rst_n : in std_logic;
   gpio1_9 : in std_logic;
   gpio1_10 : in std_logic;
   gpio1_11 : in std_logic;
   gpio1_12 : in std_logic;
   gpio2_1 : in std_logic;
   gpio2_2 : in std_logic;
   gpio2_3 : in std_logic;
   gpio2_10 : in std_logic;
   gpio2_13 : in std_logic;
   gpio2_14 : in std_logic;
   gpio3_24 : in std_logic;
   gpio3_25 : in std_logic;
   gpio3_26 : in std_logic;
   gpio3_27 : in std_logic;
   gpio4_29 : in std_logic;
   irq02 : in std_logic;
   irq08 : in std_logic;
   proc_spi_clk : in std_logic;
   proc_spi_miso : in std_logic;
   proc_spi_mosi : in std_logic;
   uart1_ctsb : in std_logic;
   cpld_rstn : in std_logic;
   gpio3_28 : in std_logic;
   gpio3_29 : in std_logic;
   gpio3_30 : in std_logic;
   gpio4_30 : in std_logic;
   gpio4_31 : in std_logic;
   hreset_n : out std_logic;
   mxm_waken : in std_logic;
   poreset_n : out std_logic;
   pwr_rst_n : in std_logic;
   reset_req_n : in std_logic;
   ddr_rst_n : out std_logic;
   irq00 : in std_logic;
   m2_rstn : out std_logic;
   mxm_rstn : out std_logic;
   nand_cs_n : out std_logic;
   nand_rb_n : in std_logic;
   nor_cs_n : out std_logic;
   nor_rst_n : out std_logic;
   pcie_bridge_rstn : out std_logic;
   pdetect : out std_logic;
   pe_waken : out std_logic;
   sata3_wake_n : out std_logic;
   sata_rstn : out std_logic;
   usb3_pewakeb : out std_logic;
   expander_rstn : out std_logic;
   vcore_en : in std_logic;
   wlan_wakeup : in std_logic;
   cfg_vbank0 : out std_logic;
   cfg_vbank1 : out std_logic;
   cfg_vbank2 : out std_logic;
   prsnt_rn : in std_logic;
   clk_50mhz : in std_logic;
   gpio3_0 : in std_logic;
   gpio3_1 : in std_logic;
   gpio3_2 : in std_logic;
   gpio3_3 : in std_logic;
   gpio3_4 : in std_logic;
   gpio3_5 : in std_logic;
   gpio3_6 : in std_logic;
   gpio3_7 : in std_logic;
   gpio4_5 : in std_logic;
   gpio4_6 : in std_logic;
   ifc_clk : in std_logic;
   ifc_cs0_n : in std_logic;
   ifc_cs1_n : in std_logic;
   ifc_cs2_n : in std_logic;
   ifc_rb0_n : out std_logic;
   ifc_rb1_n : out std_logic;
   osc_out : out std_logic;
   irq01 : in std_logic
 );
end component;

signal tmp_rst_n : std_logic;
signal tmp_gpio1_9 : std_logic;
signal tmp_gpio1_10 : std_logic;
signal tmp_gpio1_11 : std_logic;
signal tmp_gpio1_12 : std_logic;
signal tmp_gpio2_1 : std_logic;
signal tmp_gpio2_2 : std_logic;
signal tmp_gpio2_3 : std_logic;
signal tmp_gpio2_10 : std_logic;
signal tmp_gpio2_13 : std_logic;
signal tmp_gpio2_14 : std_logic;
signal tmp_gpio3_24 : std_logic;
signal tmp_gpio3_25 : std_logic;
signal tmp_gpio3_26 : std_logic;
signal tmp_gpio3_27 : std_logic;
signal tmp_gpio4_29 : std_logic;
signal tmp_irq02 : std_logic;
signal tmp_irq08 : std_logic;
signal tmp_proc_spi_clk : std_logic;
signal tmp_proc_spi_miso : std_logic;
signal tmp_proc_spi_mosi : std_logic;
signal tmp_uart1_ctsb : std_logic;
signal tmp_cpld_rstn : std_logic;
signal tmp_gpio3_28 : std_logic;
signal tmp_gpio3_29 : std_logic;
signal tmp_gpio3_30 : std_logic;
signal tmp_gpio4_30 : std_logic;
signal tmp_gpio4_31 : std_logic;
signal tmp_hreset_n : std_logic;
signal tmp_mxm_waken : std_logic;
signal tmp_poreset_n : std_logic;
signal tmp_pwr_rst_n : std_logic;
signal tmp_reset_req_n : std_logic;
signal tmp_ddr_rst_n : std_logic;
signal tmp_irq00 : std_logic;
signal tmp_m2_rstn : std_logic;
signal tmp_mxm_rstn : std_logic;
signal tmp_nand_cs_n : std_logic;
signal tmp_nand_rb_n : std_logic;
signal tmp_nor_cs_n : std_logic;
signal tmp_nor_rst_n : std_logic;
signal tmp_pcie_bridge_rstn : std_logic;
signal tmp_pdetect : std_logic;
signal tmp_pe_waken : std_logic;
signal tmp_sata3_wake_n : std_logic;
signal tmp_sata_rstn : std_logic;
signal tmp_usb3_pewakeb : std_logic;
signal tmp_expander_rstn : std_logic;
signal tmp_vcore_en : std_logic;
signal tmp_wlan_wakeup : std_logic;
signal tmp_cfg_vbank0 : std_logic;
signal tmp_cfg_vbank1 : std_logic;
signal tmp_cfg_vbank2 : std_logic;
signal tmp_prsnt_rn : std_logic;
signal tmp_clk_50mhz : std_logic;
signal tmp_gpio3_0 : std_logic;
signal tmp_gpio3_1 : std_logic;
signal tmp_gpio3_2 : std_logic;
signal tmp_gpio3_3 : std_logic;
signal tmp_gpio3_4 : std_logic;
signal tmp_gpio3_5 : std_logic;
signal tmp_gpio3_6 : std_logic;
signal tmp_gpio3_7 : std_logic;
signal tmp_gpio4_5 : std_logic;
signal tmp_gpio4_6 : std_logic;
signal tmp_ifc_clk : std_logic;
signal tmp_ifc_cs0_n : std_logic;
signal tmp_ifc_cs1_n : std_logic;
signal tmp_ifc_cs2_n : std_logic;
signal tmp_ifc_rb0_n : std_logic;
signal tmp_ifc_rb1_n : std_logic;
signal tmp_osc_out : std_logic;
signal tmp_irq01 : std_logic;

begin

tmp_rst_n <= rst_n;

tmp_gpio1_9 <= gpio1_9;

tmp_gpio1_10 <= gpio1_10;

tmp_gpio1_11 <= gpio1_11;

tmp_gpio1_12 <= gpio1_12;

tmp_gpio2_1 <= gpio2_1;

tmp_gpio2_2 <= gpio2_2;

tmp_gpio2_3 <= gpio2_3;

tmp_gpio2_10 <= gpio2_10;

tmp_gpio2_13 <= gpio2_13;

tmp_gpio2_14 <= gpio2_14;

tmp_gpio3_24 <= gpio3_24;

tmp_gpio3_25 <= gpio3_25;

tmp_gpio3_26 <= gpio3_26;

tmp_gpio3_27 <= gpio3_27;

tmp_gpio4_29 <= gpio4_29;

tmp_irq02 <= irq02;

tmp_irq08 <= irq08;

tmp_proc_spi_clk <= proc_spi_clk;

tmp_proc_spi_miso <= proc_spi_miso;

tmp_proc_spi_mosi <= proc_spi_mosi;

tmp_uart1_ctsb <= uart1_ctsb;

tmp_cpld_rstn <= cpld_rstn;

tmp_gpio3_28 <= gpio3_28;

tmp_gpio3_29 <= gpio3_29;

tmp_gpio3_30 <= gpio3_30;

tmp_gpio4_30 <= gpio4_30;

tmp_gpio4_31 <= gpio4_31;

hreset_n <= tmp_hreset_n;

tmp_mxm_waken <= mxm_waken;

poreset_n <= tmp_poreset_n;

tmp_pwr_rst_n <= pwr_rst_n;

tmp_reset_req_n <= reset_req_n;

ddr_rst_n <= tmp_ddr_rst_n;

tmp_irq00 <= irq00;

m2_rstn <= tmp_m2_rstn;

mxm_rstn <= tmp_mxm_rstn;

nand_cs_n <= tmp_nand_cs_n;

tmp_nand_rb_n <= nand_rb_n;

nor_cs_n <= tmp_nor_cs_n;

nor_rst_n <= tmp_nor_rst_n;

pcie_bridge_rstn <= tmp_pcie_bridge_rstn;

pdetect <= tmp_pdetect;

pe_waken <= tmp_pe_waken;

sata3_wake_n <= tmp_sata3_wake_n;

sata_rstn <= tmp_sata_rstn;

usb3_pewakeb <= tmp_usb3_pewakeb;

expander_rstn <= tmp_expander_rstn;

tmp_vcore_en <= vcore_en;

tmp_wlan_wakeup <= wlan_wakeup;

cfg_vbank0 <= tmp_cfg_vbank0;

cfg_vbank1 <= tmp_cfg_vbank1;

cfg_vbank2 <= tmp_cfg_vbank2;

tmp_prsnt_rn <= prsnt_rn;

tmp_clk_50mhz <= clk_50mhz;

tmp_gpio3_0 <= gpio3_0;

tmp_gpio3_1 <= gpio3_1;

tmp_gpio3_2 <= gpio3_2;

tmp_gpio3_3 <= gpio3_3;

tmp_gpio3_4 <= gpio3_4;

tmp_gpio3_5 <= gpio3_5;

tmp_gpio3_6 <= gpio3_6;

tmp_gpio3_7 <= gpio3_7;

tmp_gpio4_5 <= gpio4_5;

tmp_gpio4_6 <= gpio4_6;

tmp_ifc_clk <= ifc_clk;

tmp_ifc_cs0_n <= ifc_cs0_n;

tmp_ifc_cs1_n <= ifc_cs1_n;

tmp_ifc_cs2_n <= ifc_cs2_n;

ifc_rb0_n <= tmp_ifc_rb0_n;

ifc_rb1_n <= tmp_ifc_rb1_n;

osc_out <= tmp_osc_out;

tmp_irq01 <= irq01;



u1:   cpld_portatile_top port map (
		rst_n => tmp_rst_n,
		gpio1_9 => tmp_gpio1_9,
		gpio1_10 => tmp_gpio1_10,
		gpio1_11 => tmp_gpio1_11,
		gpio1_12 => tmp_gpio1_12,
		gpio2_1 => tmp_gpio2_1,
		gpio2_2 => tmp_gpio2_2,
		gpio2_3 => tmp_gpio2_3,
		gpio2_10 => tmp_gpio2_10,
		gpio2_13 => tmp_gpio2_13,
		gpio2_14 => tmp_gpio2_14,
		gpio3_24 => tmp_gpio3_24,
		gpio3_25 => tmp_gpio3_25,
		gpio3_26 => tmp_gpio3_26,
		gpio3_27 => tmp_gpio3_27,
		gpio4_29 => tmp_gpio4_29,
		irq02 => tmp_irq02,
		irq08 => tmp_irq08,
		proc_spi_clk => tmp_proc_spi_clk,
		proc_spi_miso => tmp_proc_spi_miso,
		proc_spi_mosi => tmp_proc_spi_mosi,
		uart1_ctsb => tmp_uart1_ctsb,
		cpld_rstn => tmp_cpld_rstn,
		gpio3_28 => tmp_gpio3_28,
		gpio3_29 => tmp_gpio3_29,
		gpio3_30 => tmp_gpio3_30,
		gpio4_30 => tmp_gpio4_30,
		gpio4_31 => tmp_gpio4_31,
		hreset_n => tmp_hreset_n,
		mxm_waken => tmp_mxm_waken,
		poreset_n => tmp_poreset_n,
		pwr_rst_n => tmp_pwr_rst_n,
		reset_req_n => tmp_reset_req_n,
		ddr_rst_n => tmp_ddr_rst_n,
		irq00 => tmp_irq00,
		m2_rstn => tmp_m2_rstn,
		mxm_rstn => tmp_mxm_rstn,
		nand_cs_n => tmp_nand_cs_n,
		nand_rb_n => tmp_nand_rb_n,
		nor_cs_n => tmp_nor_cs_n,
		nor_rst_n => tmp_nor_rst_n,
		pcie_bridge_rstn => tmp_pcie_bridge_rstn,
		pdetect => tmp_pdetect,
		pe_waken => tmp_pe_waken,
		sata3_wake_n => tmp_sata3_wake_n,
		sata_rstn => tmp_sata_rstn,
		usb3_pewakeb => tmp_usb3_pewakeb,
		expander_rstn => tmp_expander_rstn,
		vcore_en => tmp_vcore_en,
		wlan_wakeup => tmp_wlan_wakeup,
		cfg_vbank0 => tmp_cfg_vbank0,
		cfg_vbank1 => tmp_cfg_vbank1,
		cfg_vbank2 => tmp_cfg_vbank2,
		prsnt_rn => tmp_prsnt_rn,
		clk_50mhz => tmp_clk_50mhz,
		gpio3_0 => tmp_gpio3_0,
		gpio3_1 => tmp_gpio3_1,
		gpio3_2 => tmp_gpio3_2,
		gpio3_3 => tmp_gpio3_3,
		gpio3_4 => tmp_gpio3_4,
		gpio3_5 => tmp_gpio3_5,
		gpio3_6 => tmp_gpio3_6,
		gpio3_7 => tmp_gpio3_7,
		gpio4_5 => tmp_gpio4_5,
		gpio4_6 => tmp_gpio4_6,
		ifc_clk => tmp_ifc_clk,
		ifc_cs0_n => tmp_ifc_cs0_n,
		ifc_cs1_n => tmp_ifc_cs1_n,
		ifc_cs2_n => tmp_ifc_cs2_n,
		ifc_rb0_n => tmp_ifc_rb0_n,
		ifc_rb1_n => tmp_ifc_rb1_n,
		osc_out => tmp_osc_out,
		irq01 => tmp_irq01
       );
end rtl;
